<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LoggerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;

class TestApiCommand extends Command
{
    private $host = 'webserver';

    /**
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;

        parent::__construct();
    }

    protected static $defaultName = 'test:api';

    protected function configure()
    {
        $this->setDescription('Skrypt komunikuje się z API');
    }

    /**
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->info('Test API');

        $userResponsePost = $this->post('http://' . $this->host . '/api/users', [
            'name' => 'Łukasz',
            'surname' => 'Marszałek'
        ]);
        dump((string)$userResponsePost['response']->getBody());

        $productResponsePost = $this->post('http://' . $this->host . '/api/products', [
            'name' => 'Rower terenowy',
            'price' => 2220.00,
            'description' => 'Super rower'
        ]);
        dump((string)$productResponsePost['response']->getBody());

        $productResponseGet = $this->get('http://' . $this->host . '/api/products?page=1');
        dump((string)$productResponseGet['response']->getBody());

        $cartsResponsePost = $this->post('http://' . $this->host . '/api/carts', [
            'productId' => 1,
            'price' => 3500.00,
            'quantity' => 2,
            'userId' =>  1
        ]);
        dump((string)$cartsResponsePost['response']->getBody());

        $cartsResponseDelete = $this->delete('http://' . $this->host . '/api/carts/1');
        dump((string)$cartsResponseDelete['response']->getBody());

        return 0;
    }

    protected function delete($url) {
        $info = null;

        $client = new Client();
        $res = $client->request('DELETE', $url, [
            'headers' => [
                'Accept' => 'application/json',
            ],
            'http_errors' => false,
            'on_stats' => function (TransferStats $stats) use (&$info) {
                $info = $stats->getHandlerStats();
            }
        ]);

        return [
            'response' => $res,
            'info' => $info
        ];
    }

    protected function get($url) {
        $info = null;

        $client = new Client();
        $res = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
            ],
            'http_errors' => false,
            'on_stats' => function (TransferStats $stats) use (&$info) {
                $info = $stats->getHandlerStats();
            }
        ]);

        return [
            'response' => $res,
            'info' => $info
        ];
    }

    protected function post($url, $data) {
        $info = null;

        $client = new Client();
        $res = $client->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json',
            ],
            'json' => $data,
            'http_errors' => false,
            'on_stats' => function (TransferStats $stats) use (&$info) {
                $info = $stats->getHandlerStats();
            }
        ]);

        return [
            'response' => $res,
            'info' => $info
        ];
    }
}
